﻿namespace Project_booking
{
    partial class Booked
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.amountDropDown = new System.Windows.Forms.ComboBox();
            this.dateLabel = new System.Windows.Forms.Label();
            this.dateTimeBooking = new System.Windows.Forms.DateTimePicker();
            this.bookingTextBox = new System.Windows.Forms.ListBox();
            this.amountLabel = new System.Windows.Forms.Label();
            this.bookedByLabel = new System.Windows.Forms.Label();
            this.bookButton = new System.Windows.Forms.Button();
            this.goBack = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // amountDropDown
            // 
            this.amountDropDown.FormattingEnabled = true;
            this.amountDropDown.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.amountDropDown.Location = new System.Drawing.Point(314, 42);
            this.amountDropDown.Name = "amountDropDown";
            this.amountDropDown.Size = new System.Drawing.Size(162, 21);
            this.amountDropDown.TabIndex = 0;
            this.amountDropDown.SelectedIndexChanged += new System.EventHandler(this.amountDropDown_SelectedIndexChanged);
            // 
            // dateLabel
            // 
            this.dateLabel.AutoSize = true;
            this.dateLabel.Location = new System.Drawing.Point(31, 27);
            this.dateLabel.Name = "dateLabel";
            this.dateLabel.Size = new System.Drawing.Size(30, 13);
            this.dateLabel.TabIndex = 1;
            this.dateLabel.Text = "Dato";
            this.dateLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // dateTimeBooking
            // 
            this.dateTimeBooking.Location = new System.Drawing.Point(34, 43);
            this.dateTimeBooking.Name = "dateTimeBooking";
            this.dateTimeBooking.Size = new System.Drawing.Size(200, 20);
            this.dateTimeBooking.TabIndex = 3;
            // 
            // bookingTextBox
            // 
            this.bookingTextBox.FormattingEnabled = true;
            this.bookingTextBox.Location = new System.Drawing.Point(34, 117);
            this.bookingTextBox.Name = "bookingTextBox";
            this.bookingTextBox.Size = new System.Drawing.Size(200, 95);
            this.bookingTextBox.TabIndex = 4;
            this.bookingTextBox.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // amountLabel
            // 
            this.amountLabel.AutoSize = true;
            this.amountLabel.Location = new System.Drawing.Point(445, 27);
            this.amountLabel.Name = "amountLabel";
            this.amountLabel.Size = new System.Drawing.Size(31, 13);
            this.amountLabel.TabIndex = 5;
            this.amountLabel.Text = "Antal";
            this.amountLabel.Click += new System.EventHandler(this.ANTAL_Click);
            // 
            // bookedByLabel
            // 
            this.bookedByLabel.AutoSize = true;
            this.bookedByLabel.Location = new System.Drawing.Point(31, 101);
            this.bookedByLabel.Name = "bookedByLabel";
            this.bookedByLabel.Size = new System.Drawing.Size(56, 13);
            this.bookedByLabel.TabIndex = 6;
            this.bookedByLabel.Text = "Booket af:";
            // 
            // bookButton
            // 
            this.bookButton.Location = new System.Drawing.Point(234, 243);
            this.bookButton.Name = "bookButton";
            this.bookButton.Size = new System.Drawing.Size(155, 66);
            this.bookButton.TabIndex = 7;
            this.bookButton.Text = "Book nu";
            this.bookButton.UseVisualStyleBackColor = true;
            // 
            // goBack
            // 
            this.goBack.Location = new System.Drawing.Point(536, 40);
            this.goBack.Name = "goBack";
            this.goBack.Size = new System.Drawing.Size(75, 23);
            this.goBack.TabIndex = 8;
            this.goBack.Text = "Tilbage";
            this.goBack.UseVisualStyleBackColor = true;
            // 
            // Booked
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(640, 363);
            this.Controls.Add(this.goBack);
            this.Controls.Add(this.bookButton);
            this.Controls.Add(this.bookedByLabel);
            this.Controls.Add(this.amountLabel);
            this.Controls.Add(this.bookingTextBox);
            this.Controls.Add(this.dateTimeBooking);
            this.Controls.Add(this.dateLabel);
            this.Controls.Add(this.amountDropDown);
            this.Name = "Booked";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox amountDropDown;
        private System.Windows.Forms.Label dateLabel;
        private System.Windows.Forms.DateTimePicker dateTimeBooking;
        private System.Windows.Forms.ListBox bookingTextBox;
        private System.Windows.Forms.Label amountLabel;
        private System.Windows.Forms.Label bookedByLabel;
        private System.Windows.Forms.Button bookButton;
        private System.Windows.Forms.Button goBack;
    }
}