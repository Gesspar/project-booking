﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project_booking
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label3.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == textBox1.Text && textBox1.Text == "admin")
            {
                Form2 frm2 = new Form2();
                frm2.Show();
                this.Hide();
                
            }
            else
            {
                SQLiteConnection con = new SQLiteConnection("Data Source=register.db;Version=3;");
                string str = "select name, email, pass from users where email = '" + textBox1.Text + "' and pass = '" + textBox2.Text + "';";
                con.Open();
                SQLiteCommand cmd = new SQLiteCommand(str, con);
                cmd.ExecuteNonQuery();
                SQLiteDataAdapter da = new SQLiteDataAdapter(str, con);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    //Sucesfull login logic happens here.

                    SQLiteDataReader dr;
                    dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        this.Text = dr["name"].ToString(); //sets the name of the window to the logged in user.
                    }
                }
                else
                {
                    //Failure to login logic happens here....
                    label3.Show();

                }
                con.Close();
            }
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //midlertidig admin login
            this.Hide();
            FormAdminMain f = new FormAdminMain();
            f.Show();
        }
    }
}
